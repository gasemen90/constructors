package ru.sam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sam.constructors.domain.entity.DomainEntity;
import ru.sam.constructors.domain.entity.DomainEntityBuilder;
import ru.sam.constructors.integration.DomainEntityDto;
import ru.sam.constructors.integration.IntegrationDomainEntityBuilder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class App {
    static final Logger logger = LoggerFactory.getLogger(App.class);

    static final String FILE_NAME = "entity.xml";

    public static void main(String[] args) throws Exception {
        File xml = getXml(FILE_NAME);
        Unmarshaller jaxbUnmarshaller = createUnmarshaller(DomainEntityDto.class);
        DomainEntityDto domainEntityDto = (DomainEntityDto) jaxbUnmarshaller.unmarshal(xml);

        logger.debug("Unmarshalled dto-entity: {}", domainEntityDto);

        DomainEntity domainEntity = new IntegrationDomainEntityBuilder().build(domainEntityDto);

        logger.debug("Converted entity: {}", domainEntity);

        DomainEntity clonedEntity = new DomainEntityBuilder().build(domainEntity);

        logger.debug("Cloned entity: {}", clonedEntity);

    }

    static File getXml(String path) {
        return new File(App.class.getClassLoader().getResource(path).getFile());
    }

    static Unmarshaller createUnmarshaller(Class<?> clazz) throws JAXBException {
        return JAXBContext.newInstance(clazz).createUnmarshaller();
    }
}
