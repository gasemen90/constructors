package ru.sam.constructors.domain;

public interface EntityBuilder<E extends Entity, Context> {

    E build(Context context);

}
