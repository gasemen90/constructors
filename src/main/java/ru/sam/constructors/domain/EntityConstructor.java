package ru.sam.constructors.domain;

public interface EntityConstructor<Entity> {

    Entity get();

}
