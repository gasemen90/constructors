package ru.sam.constructors.domain.subentity1;

import ru.sam.constructors.domain.EntityBuilder;

public class DomainSubentity1Builder implements EntityBuilder<DomainSubentity1, DomainSubentity1> {

    @Override
    public DomainSubentity1 build(DomainSubentity1 domainSubentity1) {
        return DomainSubentity1.constructor()
                .field0(domainSubentity1.getField0())
                .field1(domainSubentity1.getField1())
                .get();
    }
}
