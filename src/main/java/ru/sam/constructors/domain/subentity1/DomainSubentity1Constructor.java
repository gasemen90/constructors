package ru.sam.constructors.domain.subentity1;

import ru.sam.constructors.domain.EntityConstructor;

public class DomainSubentity1Constructor implements EntityConstructor<DomainSubentity1> {

    private final DomainSubentity1 proto = new DomainSubentity1();

    protected DomainSubentity1Constructor() {
        // empty
    }

    public DomainSubentity1Constructor field0(String field0) {
        proto.field0 = field0;
        return this;
    }

    public DomainSubentity1Constructor field1(String field1) {
        proto.field1 = field1;
        return this;
    }

    @Override
    public DomainSubentity1 get() {
        final DomainSubentity1 subentity = new DomainSubentity1();
        subentity.field0 = proto.field0;
        subentity.field1 = proto.field1;
        return subentity;
    }
}
