package ru.sam.constructors.domain.subentity2;


import ru.sam.constructors.domain.EntityConstructor;

public class DomainSubentity2Constructor implements EntityConstructor<DomainSubentity2> {

    private final DomainSubentity2 proto = new DomainSubentity2();

    protected DomainSubentity2Constructor() {
        // empty
    }

    public DomainSubentity2Constructor field0(String field0) {
        proto.field0 = field0;
        return this;
    }

    public DomainSubentity2Constructor field1(String field1) {
        proto.field1 = field1;
        return this;
    }

    @Override
    public DomainSubentity2 get() {
        final DomainSubentity2 subentity = new DomainSubentity2();
        subentity.field0 = proto.field0;
        subentity.field1 = proto.field1;
        return subentity;
    }
}
