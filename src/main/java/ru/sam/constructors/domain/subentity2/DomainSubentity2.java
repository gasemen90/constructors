package ru.sam.constructors.domain.subentity2;

import ru.sam.constructors.domain.Entity;

import java.util.UUID;

public class DomainSubentity2 implements Entity {

    private final String id = UUID.randomUUID().toString();

    protected String field0;
    protected String field1;

    protected DomainSubentity2() {
        // empty
    }

    public static DomainSubentity2Constructor constructor() {
        return new DomainSubentity2Constructor();
    }

    @Override
    public String id() {
        return id;
    }

    public String getField0() {
        return field0;
    }

    public String getField1() {
        return field1;
    }

    public void update(DomainSubentity2 source) {
        updateField0(source.field0);
        updateField1(source.field1);
    }

    private void updateField0(String field0) {
        if (field0 != null) {
            this.field0 = field0;
        }
    }

    private void updateField1(String field1) {
        if (field1 != null) {
            this.field1 = field1;
        }
    }

    @Override
    public String toString() {
        return String.format("%s; %s", field0, field1);
    }
}
