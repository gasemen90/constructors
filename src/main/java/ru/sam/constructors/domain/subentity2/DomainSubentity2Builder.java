package ru.sam.constructors.domain.subentity2;

import ru.sam.constructors.domain.EntityBuilder;

public class DomainSubentity2Builder implements EntityBuilder<DomainSubentity2, DomainSubentity2> {

    @Override
    public DomainSubentity2 build(DomainSubentity2 domainSubentity1) {
        return DomainSubentity2.constructor()
                .field0(domainSubentity1.getField0())
                .field1(domainSubentity1.getField1())
                .get();
    }
}
