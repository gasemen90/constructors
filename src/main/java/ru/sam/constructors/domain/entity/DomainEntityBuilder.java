package ru.sam.constructors.domain.entity;

import ru.sam.constructors.domain.subentity1.DomainSubentity1Builder;
import ru.sam.constructors.domain.subentity2.DomainSubentity2Builder;

public class DomainEntityBuilder extends AbstractDomainEntityBuilder<DomainEntity> {

    @Override
    protected void buildEntity(DomainEntity proto) {
        entityConstructor
                .field0(proto.getField0())
                .field1(proto.getField1())
                .field2(proto.getField2())
                .field3(proto.getField3())
                .field4(proto.getField4())
                .field5(proto.getField5())
                .field6(proto.getField6())
                .field7(proto.getField7())
                .field8(proto.getField8())
                .field9(proto.getField9());
    }

    @Override
    protected void buildSubentity1(DomainEntity proto) {
        entityConstructor.subentity1(new DomainSubentity1Builder().build(proto.getSubentity1()));
    }

    @Override
    protected void buildSubentity2(DomainEntity proto) {
        entityConstructor.subentity2(new DomainSubentity2Builder().build(proto.getSubentity2()));
    }
}
