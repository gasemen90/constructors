package ru.sam.constructors.domain.entity;

import ru.sam.constructors.domain.EntityBuilder;

public abstract class AbstractDomainEntityBuilder<Context> implements EntityBuilder<DomainEntity, Context> {

    protected DomainEntityConstructor entityConstructor = DomainEntity.constructor();

    @Override
    public DomainEntity build(Context context) {
        buildEntity(context);
        buildSubentity1(context);
        buildSubentity2(context);
        return entityConstructor.get();
    }

    protected abstract void buildEntity(Context context);

    protected abstract void buildSubentity1(Context context);

    protected abstract void buildSubentity2(Context context);
}
