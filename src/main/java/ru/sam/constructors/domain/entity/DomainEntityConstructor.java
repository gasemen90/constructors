package ru.sam.constructors.domain.entity;

import ru.sam.constructors.domain.EntityConstructor;
import ru.sam.constructors.domain.subentity1.DomainSubentity1;
import ru.sam.constructors.domain.subentity2.DomainSubentity2;

public class DomainEntityConstructor implements EntityConstructor<DomainEntity> {

    private final DomainEntity proto = new DomainEntity();

    protected DomainEntityConstructor() {
        // empty
    }

    public DomainEntityConstructor field0(String field0) {
        proto.field0 = field0;
        return this;
    }

    public DomainEntityConstructor field1(String field1) {
        proto.field1 = field1;
        return this;
    }

    public DomainEntityConstructor field2(String field2) {
        proto.field2 = field2;
        return this;
    }

    public DomainEntityConstructor field3(String field3) {
        proto.field3 = field3;
        return this;
    }

    public DomainEntityConstructor field4(String field4) {
        proto.field4 = field4;
        return this;
    }

    public DomainEntityConstructor field5(String field5) {
        proto.field5 = field5;
        return this;
    }

    public DomainEntityConstructor field6(String field6) {
        proto.field6 = field6;
        return this;
    }

    public DomainEntityConstructor field7(String field7) {
        proto.field7 = field7;
        return this;
    }

    public DomainEntityConstructor field8(String field8) {
        proto.field8 = field8;
        return this;
    }

    public DomainEntityConstructor field9(String field9) {
        proto.field9 = field9;
        return this;
    }

    public DomainEntityConstructor subentity1(DomainSubentity1 subentity1) {
        proto.subentity1 = subentity1;
        return this;
    }

    public DomainEntityConstructor subentity2(DomainSubentity2 subentity2) {
        proto.subentity2 = subentity2;
        return this;
    }

    @Override
    public DomainEntity get() {
        final DomainEntity domainEntity = new DomainEntity();
        domainEntity.field0 = proto.field0;
        domainEntity.field1 = proto.field1;
        domainEntity.field2 = proto.field2;
        domainEntity.field3 = proto.field3;
        domainEntity.field4 = proto.field4;
        domainEntity.field5 = proto.field5;
        domainEntity.field6 = proto.field6;
        domainEntity.field7 = proto.field7;
        domainEntity.field8 = proto.field8;
        domainEntity.field9 = proto.field9;
        domainEntity.subentity1 = proto.subentity1;
        domainEntity.subentity2 = proto.subentity2;
        return domainEntity;
    }

}
