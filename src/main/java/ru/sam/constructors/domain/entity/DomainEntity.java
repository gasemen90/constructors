package ru.sam.constructors.domain.entity;


import ru.sam.constructors.domain.Entity;
import ru.sam.constructors.domain.subentity1.DomainSubentity1;
import ru.sam.constructors.domain.subentity2.DomainSubentity2;

import java.util.UUID;

public class DomainEntity implements Entity {

    private final String id = UUID.randomUUID().toString();

    protected String field0;
    protected String field1;
    protected String field2;
    protected String field3;
    protected String field4;
    protected String field5;
    protected String field6;
    protected String field7;
    protected String field8;
    protected String field9;
    protected DomainSubentity1 subentity1;
    protected DomainSubentity2 subentity2;

    protected DomainEntity() {
        // empty
    }

    public static DomainEntityConstructor constructor() {
        return new DomainEntityConstructor();
    }

    @Override
    public String id() {
        return id;
    }

    public String getField0() {
        return field0;
    }

    public String getField1() {
        return field1;
    }

    public String getField2() {
        return field2;
    }

    public String getField3() {
        return field3;
    }

    public String getField4() {
        return field4;
    }

    public String getField5() {
        return field5;
    }

    public String getField6() {
        return field6;
    }

    public String getField7() {
        return field7;
    }

    public String getField8() {
        return field8;
    }

    public String getField9() {
        return field9;
    }

    public DomainSubentity1 getSubentity1() {
        return subentity1;
    }

    public DomainSubentity2 getSubentity2() {
        return subentity2;
    }

    public void update(DomainEntity source) {
        updateField0(source.field0);
        updateField1(source.field1);
        updateField2(source.field2);
        updateField3(source.field3);
        updateField4(source.field4);
        updateField5(source.field5);
        updateField6(source.field6);
        updateField7(source.field7);
        updateField8(source.field8);
        updateField9(source.field9);
        updateSubentity1(source.subentity1);
        updateSubentity2(source.subentity2);
    }

    private void updateField0(String field0) {
        if (field0 != null) {
            this.field0 = field0;
        }
    }

    private void updateField1(String field1) {
        if (field1 != null) {
            this.field1 = field1;
        }
    }

    private void updateField2(String field2) {
        if (field2 != null) {
            this.field2 = field2;
        }
    }

    private void updateField3(String field3) {
        if (field3 != null) {
            this.field3 = field3;
        }
    }

    private void updateField4(String field4) {
        if (field4 != null) {
            this.field4 = field4;
        }
    }

    private void updateField5(String field5) {
        if (field5 != null) {
            this.field5 = field5;
        }
    }

    private void updateField6(String field6) {
        if (field6 != null) {
            this.field6 = field6;
        }
    }

    private void updateField7(String field7) {
        if (field7 != null) {
            this.field7 = field7;
        }
    }

    private void updateField8(String field8) {
        if (field8 != null) {
            this.field8 = field8;
        }
    }

    private void updateField9(String field9) {
        if (field9 != null) {
            this.field9 = field9;
        }
    }

    private void updateSubentity1(DomainSubentity1 subentity1) {
        if (this.subentity1 == null) {
            this.subentity1 = subentity1;
        } else {
            this.subentity1.update(subentity1);
        }
    }

    private void updateSubentity2(DomainSubentity2 subentity2) {
        if (this.subentity2 == null) {
            this.subentity2 = subentity2;
        } else {
            this.subentity2.update(subentity2);
        }
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(field0).append("; ")
                .append(field1).append("; ")
                .append(field2).append("; ")
                .append(field3).append("; ")
                .append(field4).append("; ")
                .append(field5).append("; ")
                .append(field6).append("; ")
                .append(field7).append("; ")
                .append(field8).append("; ")
                .append(field9).append("; ")
                .append(subentity1).append("; ")
                .append(subentity2).toString();
    }
}