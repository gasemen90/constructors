package ru.sam.constructors.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "entity")
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainEntityDto implements Serializable {

    public String field0;

    public String field1;

    public String field2;

    public String field3;

    public String field4;

    public String field5;

    public String field6;

    public String field7;

    public String field8;

    public String field9;

    public DomainSubentity1Dto subentity1;

    public DomainSubentity2Dto subentity2;


    @Override
    public String toString() {
        return new StringBuilder()
                .append(field0).append("; ")
                .append(field1).append("; ")
                .append(field2).append("; ")
                .append(field3).append("; ")
                .append(field4).append("; ")
                .append(field5).append("; ")
                .append(field6).append("; ")
                .append(field7).append("; ")
                .append(field8).append("; ")
                .append(field9).append("; ")
                .append(subentity1).append("; ")
                .append(subentity2).append("; ")
                .toString();
    }
}
