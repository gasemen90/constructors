package ru.sam.constructors.integration;

import com.sun.xml.internal.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;

@XmlElement("subentity1")
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainSubentity2Dto implements Serializable {

    public String field0;

    public String field1;

    @Override
    public String toString() {
        return String.format("%s; %s", field0, field1);
    }

}
