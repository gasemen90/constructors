package ru.sam.constructors.integration;

import ru.sam.constructors.domain.EntityBuilder;
import ru.sam.constructors.domain.subentity1.DomainSubentity1;

public class DomainSubentity1DtoBuilder implements EntityBuilder<DomainSubentity1, DomainSubentity1Dto> {

    @Override
    public DomainSubentity1 build(DomainSubentity1Dto dto) {
        return DomainSubentity1.constructor()
                .field0(dto.field0)
                .field1(dto.field1)
                .get();
    }
}
