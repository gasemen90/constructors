package ru.sam.constructors.integration;

import ru.sam.constructors.domain.entity.AbstractDomainEntityBuilder;

public class IntegrationDomainEntityBuilder extends AbstractDomainEntityBuilder<DomainEntityDto> {

    @Override
    protected void buildEntity(DomainEntityDto dto) {
        entityConstructor.field0(dto.field0);
        entityConstructor.field1(dto.field1);
        entityConstructor.field2(dto.field2);
        entityConstructor.field3(dto.field3);
        entityConstructor.field4(dto.field4);
        entityConstructor.field5(dto.field5);
        entityConstructor.field6(dto.field6);
        entityConstructor.field7(dto.field7);
        entityConstructor.field8(dto.field8);
        entityConstructor.field9(dto.field9);
    }

    @Override
    protected void buildSubentity1(DomainEntityDto domainEntityDto) {
        entityConstructor.subentity1(new DomainSubentity1DtoBuilder().build(domainEntityDto.subentity1));
    }

    @Override
    protected void buildSubentity2(DomainEntityDto domainEntityDto) {
        entityConstructor.subentity2(new DomainSubentity2DtoBuilder().build(domainEntityDto.subentity2));
    }
}
