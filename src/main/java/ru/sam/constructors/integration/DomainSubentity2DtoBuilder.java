package ru.sam.constructors.integration;

import ru.sam.constructors.domain.EntityBuilder;
import ru.sam.constructors.domain.subentity2.DomainSubentity2;

public class DomainSubentity2DtoBuilder implements EntityBuilder<DomainSubentity2, DomainSubentity2Dto> {

    @Override
    public DomainSubentity2 build(DomainSubentity2Dto dto) {
        return DomainSubentity2.constructor()
                .field0(dto.field0)
                .field1(dto.field1)
                .get();
    }
}
